#include "Arduino.h"
#include "LunegateRFID.h"
#include <SPI.h>
#include <MFRC522.h>


MFRC522 mfrc522; 
MFRC522::MIFARE_Key key;
MFRC522::StatusCode status;

//----- CONSTRUCTOR ------

LunegateRFID::LunegateRFID(bool isWemos) {
    pinRST = D0;
    pinSDA = D8;
    configurate(isWemos);
}

LunegateRFID::LunegateRFID(bool isWemos, int reset, int sda) {
    pinRST = reset;
    pinSDA = sda;
    configurate(isWemos);
}

//----- PRIVATE METHOD -----

void LunegateRFID::configurate(bool isWemos) {
    if (isWemos) {
        pinSCK = 5;
        pinMOSI = 7;
        pinMISO = 6;
    } else {
        pinSCK = 0;
        pinMOSI = 0;
        pinMISO = 0;
    }
    MFRC522 mfrc522(pinSDA, pinRST); 

   SPI.begin();
   mfrc522.PCD_Init();
 
   for (byte i = 0; i < 6; i++) {
      key.keyByte[i] = 0xFF;
   }
}

bool LunegateRFID::isPrepare() {
    if (!mfrc522.PICC_IsNewCardPresent()) {
      return false;
    }
    
    if (!mfrc522.PICC_ReadCardSerial()) {
      return false;
    }
    
    return true;
}

bool LunegateRFID::isAuthenticate() {  
   status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 7, &key, &(mfrc522.uid));
   if (status != MFRC522::STATUS_OK) {
      Serial.print(F("PCD_Authenticate() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
      return false;
   }

   return true;
}

String LunegateRFID::cutText(int index, String textComplete) {
  String cutedString = textComplete.substring(index*15, index*15+15);
  return cutedString;
}

bool LunegateRFID::continueCut(String text, int index) {
  return (cutText(index, text)).length() > 0;
}


bool LunegateRFID::writeIntoRFID(String str, byte block) {    
    int str_len = str.length() + 1; 
    byte data[str_len];     
    str.getBytes(data, str_len);

   status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(block, data, sizeof(data));
   if (status != MFRC522::STATUS_OK) {
      Serial.print(F("MIFARE_Write() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
   }

   return status == MFRC522::STATUS_OK;
}

void LunegateRFID::clearAllBloks() {
    writeIntoRFID("...............", 4);
    writeIntoRFID("...............", 5);
    writeIntoRFID("...............", 6);
}

String LunegateRFID::readRFIDIntoBlock(int block) {

     //-- Read --
     int sizeBuffer = 18;
     byte buffer[sizeBuffer]; 
     byte size = sizeof(buffer);

    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(block, buffer, &size);
     if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
     }

    String stringRecover = String((char *)buffer);
    return stringRecover;
}


String LunegateRFID::parseRead(String stringBlock, String total) {

    if (stringBlock != "...............") {
       if (stringBlock.indexOf("{") > 0) {
          String cutedString = stringBlock.substring(0, stringBlock.indexOf("{"));
          total = total + cutedString; 
       } else {
          total = total + stringBlock; 
       }
    } else {
         Serial.println("BLOQUE VACIO 4");
    }

    return total;
}


String LunegateRFID::formatText(String text, int index) {
    String str = cutText(index, text);

    if (str.length() < 15) {
        int totalAddCharacter = 15 - str.length();
        for (int pos = 0; pos < totalAddCharacter; pos++) {
           str = str + "{";
        }
    }

    return str;
}

//----- PUBLIC METHOD -----


void LunegateRFID::writeRFID(String textComplete) {
    if (textComplete.length() <= 39) {
        //--  Config --
        if (!isPrepare() || !isAuthenticate()) {
            return;
        }

        //-- WRITE --
        clearAllBloks();
        int index = 0;

        
        while (continueCut(textComplete, index) == true) {
            byte block = 4 + index; 
        
            String str = formatText(textComplete, index);  

            if (writeIntoRFID(str, block) == true) { 
                Serial.print(F("ESCRITO CORRECTAMENTE en bloque: "));
                Serial.println(block);
            } else {
                Serial.print(F("FALLO DE ESCRITURA en bloque: "));
                Serial.println(block);
            }
            
            index++;
        }
    
        // -- Stop --    
        mfrc522.PICC_HaltA();
        mfrc522.PCD_StopCrypto1();        
    } else {
      Serial.println("El texto es demasiado largo");
    }
}


String LunegateRFID::readRFID() {
  
    //--  Config --
    if (!isPrepare() || !isAuthenticate()) {
        return "";
    }

      //-- Read --
    String stringBlock4 = readRFIDIntoBlock(4);
    String stringBlock5 = readRFIDIntoBlock(5);
    String stringBlock6 = readRFIDIntoBlock(6);

    //-- Parse --
    String total = "";
    total = parseRead(stringBlock4, total);
    total = parseRead(stringBlock5, total);
    total = parseRead(stringBlock6, total);

    // -- Stop --    
    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();  

    return total;
}
