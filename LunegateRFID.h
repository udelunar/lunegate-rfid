#ifndef LunegateRFID_h
#define LunegateRFID_h
#include "Arduino.h"


/*********************************************************** LICENCE ********************************************************
/****************************************************************************************************************************
//------------------------------------                   WWW.LUNEGATE.COM              --------------------------------------
/****************************************************************************************************************************
This software is owned by Lunegate.com. You can use it as long as you do not want to get a lucrative end on this software.
/****************************************************************************************************************************

/*********************** ATENCION *********************
pista 4, 5 y 6 Permite escribir
pista 7 rompe las NFC
pista 3,8,9,10, 12 no permite escribir
*******************************************************/

class LunegateRFID
{
    private:
        int pinRST;
        int pinSDA;
        int pinSCK;
        int pinMOSI;
        int pinMISO;
        
        void configurate(bool isWemos);
        bool isPrepare();
        bool isAuthenticate();
        String cutText(int index, String textComplete);
        bool continueCut(String text, int index);
        bool writeIntoRFID(String str, byte block);
        void clearAllBloks();
        String readRFIDIntoBlock(int block);
        String parseRead(String stringBlock, String total);
        String formatText(String text, int index);
                                                    
    public:
        LunegateRFID(bool isWemos);
        LunegateRFID(bool isWemos, int reset, int sda);

        //-- Public method --
        void writeRFID(String text);
        String readRFID();

};
#endif
